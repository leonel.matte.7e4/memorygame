package lion.itb.memorygame.model

import lion.itb.memorygame.R

class CardProvider {

    companion object{

        private val cards = arrayOf(
            Card(R.drawable.computer_case, "Chassis"),
            Card(R.drawable.cpu, "cpu"),
            Card(R.drawable.gpu, "gpu"),
            Card(R.drawable.hdd, "hdd"),
            Card(R.drawable.keyboard, "keyboard"),
            Card(R.drawable.mouse, "mouse"),
            Card(R.drawable.psu, "psu"),
            Card(R.drawable.ram, "ram")
        )

        fun random(): Card{
            return cards[(cards.indices).random()]
        }

    }

}