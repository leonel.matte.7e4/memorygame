package lion.itb.memorygame.model

data class Card(val image: Int, val cardName: String)