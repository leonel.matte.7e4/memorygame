package lion.itb.memorygame.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import lion.itb.memorygame.R
import lion.itb.memorygame.viewmodel.CardViewModel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val cardViewModel: CardViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cardViewModel.card0.observe(this, {
            binding
        })



    }
}