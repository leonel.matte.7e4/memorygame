package lion.itb.memorygame.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import lion.itb.memorygame.R

class GameScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_screen)
    }
}