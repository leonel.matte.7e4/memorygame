package lion.itb.memorygame.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import lion.itb.memorygame.model.Card
import lion.itb.memorygame.model.CardProvider

class CardViewModel: ViewModel() {

    val card0 = MutableLiveData<Card>()
    val card1 = MutableLiveData<Card>()
    val card2 = MutableLiveData<Card>()
    val card3 = MutableLiveData<Card>()
    val card4 = MutableLiveData<Card>()
    val card5 = MutableLiveData<Card>()
    val card6 = MutableLiveData<Card>()
    val card7 = MutableLiveData<Card>()

    fun randomCards(){
        val newCard = CardProvider.random()
        card0.postValue(newCard)
        card1.postValue(newCard)
        card2.postValue(newCard)
        card3.postValue(newCard)
        card4.postValue(newCard)
        card5.postValue(newCard)
        card6.postValue(newCard)
        card7.postValue(newCard)
    }
}